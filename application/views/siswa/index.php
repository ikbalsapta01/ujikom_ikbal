             <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">My profile</h1>

                </div>

                <div class="card mb-3" style="max-width: 550px;">
                <div class="row g-0">
                <div class="col-md-4">
                <img src="<?= base_url('assets/img/profile/') . $siswa['image']; ?>" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-md-8">
                <div class="card-body">

                <h6 class="card-title">NAMA :</h6>
                <h5 class="card-title"><?= $siswa['nama']; ?></h5>

                <h6 class="card-title">ALAMAT  :</h6>
                 <h5 class="card-text"><?= $siswa['alamat']; ?></h5>
                </div>
                </div>
                </div>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            