             <!-- Begin Page Content -->
             <div class="container-fluid">

<!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"></h1>

        <div class="row">
            <div class="col-lg">

            <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newPembayaranModal">Edit Pembayaran</a>

            <?= form_error('pembayaran/read', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                 <?= $this->session->flashdata('message'); ?>

            </div>

        </div>

    </div>

<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- MODAL -->

    <!-- Modal -->
    <div class="modal fade" id="newPembayaranModal" tabindex="-1" aria-labelledby="newPembayaranModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="newPembayaranModalLabel">Pembayaran</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="<?= base_url('pembayaran/update'); ?>" method="post">
        <div class="modal-body">

        <?php $row = $query->result(); ?>
          
        <div class="form-group">
                <input type="hidden" class="form-control" id="id_pembayaran" name="id_pembayaran" placeholder="ID pembayaran" value="<?= $row[0]->id_pembayaran ?>">
        </div>

        <div class="form-group">
                <input type="hidden" class="form-control" id="id_petugas" name="id_petugas" placeholder="ID petugas" value="<?= $row[0]->id_petugas ?>">
        </div>
                
        <div class="form-group">
            <input type="number" class="form-control" id="nisn" name="nisn" placeholder="NISN" value="<?= $row[0]->nisn ?>">
        </div>

            <div class="form-group">
                  <input type="date" class="form-control" id="tgl_bayar" name="tgl_bayar" placeholder="Tanggal Bayar" value="<?= $row[0]->tgl_bayar ?>">
            </div>

            <div class="form-group">
              <option value="">Select Bulan</option>
                <select name="bulan_dibayar" id="bulan_dibayar" class="form-control" >
                <<option value="januari" <?php if ($row[0]->bulan_dibayar == 'januari') {
                                                                                echo 'selected';
                                                                            } ?>>januari</option>

                <option value="februari" <?php if ($row[0]->bulan_dibayar == 'februari') {
                                                                                echo 'selected';
                                                                            } ?>>februari</option>

                <option value="maret" <?php if ($row[0]->bulan_dibayar == 'maret') {
                                                                                echo 'selected';
                                                                            } ?>>maret</option>

                <option value="april" <?php if ($row[0]->bulan_dibayar == 'april') {
                                                                                echo 'selected';
                                                                            } ?>>april</option>

                <option value="mei" <?php if ($row[0]->bulan_dibayar == 'mei') {
                                                                                echo 'selected';
                                                                            } ?>>mei</option>

                <option value="juni" <?php if ($row[0]->bulan_dibayar == 'juni') {
                                                                                echo 'selected';
                                                                            } ?>>juni</option>

                <option value="juli" <?php if ($row[0]->bulan_dibayar == 'juli') {
                                                                                echo 'selected';
                                                                            } ?>>juli</option>

                <option value="agustus" <?php if ($row[0]->bulan_dibayar == 'agustus') {
                                                                                echo 'selected';
                                                                            } ?>>agustus</option>

                <option value="september" <?php if ($row[0]->bulan_dibayar == 'september') {
                                                                                echo 'selected';
                                                                            } ?>>september</option>

                <option value="oktober" <?php if ($row[0]->bulan_dibayar == 'oktober') {
                                                                                echo 'selected';
                                                                            } ?>>oktober</option>

                <option value="november" <?php if ($row[0]->bulan_dibayar == 'november') {
                                                                                echo 'selected';
                                                                            } ?>>november</option>
                <option value="desember" <?php if ($row[0]->bulan_dibayar == 'desember') {
                                                                                echo 'selected';
                                                                            } ?>>desember</option>

               </select>
            </div>

            <div class="form-group">
              <option value="">Select Tahun</option>
                <select name="tahun_dibayar" id="tahun_dibayar" class="form-control">
                <option value="2021" <?php if ($row[0]->tahun_dibayar == '2021') {
                                                                                echo 'selected';
                                                                            } ?>>2021</option>

                <option value="2022" <?php if ($row[0]->tahun_dibayar == '2022') {
                                                                                echo 'selected';
                                                                            } ?>>2022</option>
                <option value="2023" <?php if ($row[0]->tahun_dibayar == '2023') {
                                                                                echo 'selected';
                                                                            } ?>>2023</option>
                </select>
            </div>   

            <div class="form-group">
               <option value="">Pilih SPP </option>
                <select name="id_spp" id="id_spp" class="form-control">
                <option value="1" <?php if ($row[0]->tahun_dibayar == '1') {
                                                                                echo 'selected';
                                                                            } ?>>1</option>

                <option value="2" <?php if ($row[0]->id_spp == '2') {
                                                                                echo 'selected';
                                                                            } ?>>2</option>
                
                </select>
            </div> 

            <div class="form-group">
              <option value="">Jumlah Bayar </option>
                <select name="jumlah_bayar" id="jumlah_bayar" class="form-control">
                <option value="450000" <?php if ($row[0]->jumlah_bayar == '450000') {
                                                                                echo 'selected';
                                                                            } ?>>450000</option>

                <option value="90000" <?php if ($row[0]->jumlah_bayar == '900000') {
                                                                                echo 'selected';
                                                                            } ?>>900000</option>
                
                </select>
            </div>   

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Edit Pembayaran</button>
        </div>
        </form>
        </div>
    </div>
    </div>


