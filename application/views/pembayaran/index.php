             <!-- Begin Page Content -->
             <div class="container-fluid">

<!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="row">
        <div class="col-lg">

      <?= form_error('pembayaran', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

        <?= $this->session->flashdata('message'); ?>

        <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newPembayaranModal">Silahkan Pembayaran</a>

        <table class="table table-hover">
          <thead>
              <tr> 

                  <th scope="col">NISN</th>
                  <th scope="col">Tanggal bayar</th>
                  <th scope="col">Bulan dibayar</th>
                  <th scope="col">Tahun dibayar</th>
                  <th scope="col">Jumlah bayar</th>
                  <th scope="col">Action</th>
                  
              </tr>
           </thead>
           <tbody>
               
               <?php foreach($pembayaran as $p) : ?>
               <tr>
                   <td><?= $p['nisn']; ?></td>
                   <td><?= $p['tgl_bayar']; ?></td>
                   <td><?= $p['bulan_dibayar']; ?></td>
                   <td><?= $p['tahun_dibayar']; ?></td>
                   <td><?= $p['jumlah_bayar']; ?></td>
                    <td>
                          <a href="<?= base_url('pembayaran/delete/') . $p['id_pembayaran']; ?>"><div class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></div>    
                          <a href="<?= base_url('pembayaran/read/') . $p['id_pembayaran']; ?>"><div class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></div>  
                   </td>
               </tr>
               
               <?php endforeach; ?> 
            </tbody>
        </table>

      </div>
  </div>


        </div>

<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

    <!-- MODAL -->

    <!-- Modal -->
    <div class="modal fade" id="newPembayaranModal" tabindex="-1" aria-labelledby="newPembayaranModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="newPembayaranModalLabel">Pembayaran</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="<?= base_url('pembayaran/index'); ?>" method="post">
        <div class="modal-body">
          
        <div class="form-group">
                <input type="hidden" class="form-control" id="id_petugas" name="id_petugas" placeholder="ID petugas" value="<?= $siswa['nisn'] ?>">
        </div>
                
        <div class="form-group">
            <input type="number" class="form-control" id="nisn" name="nisn" placeholder="NISN">
        </div>

            <div class="form-group">
                  <input type="date" class="form-control" id="tgl_bayar" name="tgl_bayar" placeholder="Tanggal Bayar">
            </div>

            <div class="form-group">
               <select name="bulan_dibayar" id="bulan_dibayar" class="form-control">
                <option value="">Select Bulan</option>
               
                <option value="januari" >Januari</option>
                <option value="februari" >Februari</option>
                <option value="maret" >Maret</option>
                <option value="april" >April</option>
                <option value="mei" >Mei</option>
                <option value="juni" >Juni</option>
                <option value="juli" >Juli</option>
                <option value="agustus" >Agustus</option>
                <option value="september" >September</option>
                <option value="oktober" >Oktober</option>
                <option value="november" >November</option>
                <option value="desember" >Desember</option>

               </select>
            </div>

            <div class="form-group">
               <select name="tahun_dibayar" id="tahun_dibayar" class="form-control">
                <option value="">Select Tahun</option>

                <option value="2021" >2021</option>
                <option value="2022" >2022</option>
                <option value="2023" >2023</option>
                </select>
            </div>   

            <div class="form-group">
               <select name="id_spp" id="id_spp" class="form-control">
                <option value="">Pilih SPP </option>

                <option value="1" >1</option>
                <option value="2" >2</option>
                
                </select>
            </div> 

            <div class="form-group">
               <select name="jumlah_bayar" id="jumlah_bayar" class="form-control">
                <option value="">Jumlah Bayar </option>

                <option value="450000" >450000</option>
                <option value="900000" >900000</option>
                
                </select>
            </div>   

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Bayar</button>
        </div>
        </form>
        </div>
    </div>
    </div>

