             <!-- Begin Page Content -->
             <div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

  <div class="row">
      <div class="col-lg-6">

      <?= form_error('role', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

      <?= $this->session->flashdata('message'); ?>

        <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newRoleModal">Add New Role</a>

        <table class="table table-hover">
          <thead>
              <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Role</th>
                  <th scope="col">Action</th>
                  
              </tr>
           </thead>
           <tbody>
               <?php foreach($role as $r) : ?>
               <tr>
                   <td><?= $r['id']; ?></td>
                   <td><?= $r['role']; ?></td>
                   <td>
                       <a href="<?= base_url('admin/roleaccess/') . $r['id']; ?>"class="badge badge-warning">Access</a>
                       <div class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></div>
                       <a href="delete/<?= $r['id']; ?>"><div class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></div>
                   </td>
               </tr>
               <?php endforeach; ?>
            </tbody>
        </table>

      </div>
  </div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->



<!-- MODAL -->

    <!-- Modal -->
    <div class="modal fade" id="newRoleModal" tabindex="-1" aria-labelledby="newRoleModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="newRoleModalLabel">Add New Role</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="<?= base_url('admin/role'); ?>" method="post">
        <div class="modal-body">
              
                <div class="form-group">
                  <input type="hidden" class="form-control" id="role" name="id" placeholder="Id Role">
                </div>
            
              <div class="form-group">
                  <input type="text" class="form-control" id="role" name="role" placeholder="Role name">
              </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">tambah</button>
        </div>
        </form>
        </div>
    </div>
    </div>

