<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

         <div class="row">
             <div class="col-lg">

             <table class="table table-hover">
                <thead>
                    <tr>

                        <th scope="col">NISN</th>
                        <th scope="col">Tanggal bayar</th>
                        <th scope="col">Bulan dibayar</th>
                        <th scope="col">Tahun dibayar</th>
                        <th scope="col">Jumlah bayar</th>
                         
                      </tr>
                    </thead>
                
                <tbody>

                 <?php foreach($pembayaran as $p) : ?>
                   <tr>
                        <td><?= $p['nisn']; ?></td>
                        <td><?= $p['tgl_bayar']; ?></td>
                        <td><?= $p['bulan_dibayar']; ?></td>
                        <td><?= $p['tahun_dibayar']; ?></td>
                        <td><?= $p['jumlah_bayar']; ?></td>
                   </tr>

                <?php endforeach; ?> 

                   </tbody>
                </table>
             </div>
         </div>
    </div>

<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

