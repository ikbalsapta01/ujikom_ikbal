             <!-- Begin Page Content -->
             <div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

  <div class="row">
      <div class="col-lg-6">

      <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

      <?= $this->session->flashdata('message'); ?>

        <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newMenuModal">Add New Menu</a>

        <table class="table table-hover">
          <thead>
              <tr>
                  <th scope="col">ID_Menu</th>
                  <th scope="col">Menu</th>
                  <th scope="col">Action</th>
                  
              </tr>
           </thead>
           <tbody>
               <?php foreach($menu as $m) : ?>
               <tr>
                   <td><?= $m['id']; ?></td>
                   <td><?= $m['menu']; ?></td>
                   <td>
                       <a href="menu/delete/<?= $m['id']; ?>"><div class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></div>   
                       <div class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></div>
                       
                   </td>
               </tr>
               <?php endforeach; ?>
            </tbody>
        </table>

      </div>
  </div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->



<!-- MODAL -->

    <!-- Modal -->
    <div class="modal fade" id="newMenuModal" tabindex="-1" aria-labelledby="newMenuModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="newMenuModalLabel">Add New Menu</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="<?= base_url('menu'); ?>" method="post">
        <div class="modal-body">
             
              <div class="form-group">
                  <input type="hidden" class="form-control" id="menu" name="id" placeholder="ID Menu">
              </div>

              <div class="form-group">
                  <input type="text" class="form-control" id="menu" name="menu" placeholder="Menu name">
              </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">tambah</button>
        </div>
        </form>
        </div>
    </div>
    </div>

