             <!-- Begin Page Content -->
             <div class="container-fluid">

<!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="row">
        <div class="col-lg">

      <?= form_error('datasiswa', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

        <?= $this->session->flashdata('message'); ?>

        <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newDataSiswaModal">New Data Siswa</a>

        <table class="table table-hover">
          <thead>
              <tr>         
                  <th scope="col">NISN</th>
                  <th scope="col">NIS</th>
                  <th scope="col">NAMA</th>
                  <th scope="col">ALAMAT</th>
                  <th scope="col">TELEPON</th>
                  <th scope="col">Action</th>
                  
              </tr>
           </thead>
           <tbody>
               
               <?php foreach($datasiswa as $s) : ?>
               <tr>
                   <td><?= $s['nisn']; ?></td>
                   <td><?= $s['nis']; ?></td>
                   <td><?= $s['nama']; ?></td>
                   <td><?= $s['alamat']; ?></td>
                   <td><?= $s['no_telp']; ?></td>
                    <td>
                         <a href="delete/<?= $s['nisn']; ?>"><div class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></div> 
                         <a href=""><div class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></div> 
                   </td>
               </tr>
               
               <?php endforeach; ?> 
            </tbody>
        </table>

      </div>
  </div>

        </div>

<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- MODAL -->

    <!-- Modal -->
    <div class="modal fade" id="newDataSiswaModal" tabindex="-1" aria-labelledby="newDataSiswaModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="newDataSiswaModalLabel">Add New Kelas</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="<?= base_url('datasiswa'); ?>" method="post">
        <div class="modal-body">
             
               <div class="form-group">
                  <input type="text" class="form-control" id="nisn" name="nisn" placeholder="NISN">
               </div>

              <div class="form-group">
                  <input type="text" class="form-control" id="nis" name="nis" placeholder="NIS">
              </div>

              <div class="form-group">
                  <input type="text" class="form-control" id="nama" name="nama" placeholder="NAMA">
              </div>

              <div class="form-group">
                  <input type="text" class="form-control" id="alamat" name="alamat" placeholder="ALAMAT">
              </div>

              <div class="form-group">
                  <input type="text" class="form-control" id="no_telp" name="no_telp" placeholder="TELEPON">
              </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">tambah</button>
        </div>
        </form>
        </div>
    </div>
    </div>




