             <!-- Begin Page Content -->
             <div class="container-fluid">

<!-- Page Heading -->

        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="row">
            <div class="col-lg-6">

            <?= form_error('spp', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

            <?= $this->session->flashdata('message'); ?>

            <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newSppModal">Add New Spp</a>

            <table class="table table-hover">
                <thead>
                    <tr>
                        
                        <th scope="col">Tahun</th>
                        <th scope="col">Nominal</th>
                        <th scope="col">Action</th>
                        
                    </tr>
                </thead>
                <tbody>                 
                    <?php foreach($spp as $s) : ?>
                    <tr>
                        
                        <td><?= $s['tahun']; ?></td>
                        <td><?= $s['nominal']; ?></td>
                        <td>

                            <a href="<?= base_url('spp/delete/') . $s['id_spp']; ?>"><div class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></div>    
                            <a href="<?= base_url('spp/read/') . $s['id_spp']; ?>"><div class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></div>  

                        </td>
                    </tr>                   
                    <?php endforeach; ?>
                </tbody>
            </table>

            </div>
        </div>

        </div>

<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

    <!-- MODAL -->

    <!-- Modal -->
    <div class="modal fade" id="newSppModal" tabindex="-1" aria-labelledby="newSppModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="newSppModalLabel">Add New Spp</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="<?= base_url('spp/index'); ?>" method="post">
        <div class="modal-body">
             
           <div class="form-group">
                  <input type="hidden" class="form-control" id="spp" name="id_spp" placeholder="ID_SPP">
            </div>

              <div class="form-group">
                  <input type="number" class="form-control" id="spp" name="tahun" placeholder="Spp Tahun">
            </div>

            <div class="form-group">
                  <input type="number" class="form-control" id="spp" name="nominal" placeholder="Spp Nominal">
            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">tambah</button>
        </div>
        </form>
        </div>
    </div>
    </div>