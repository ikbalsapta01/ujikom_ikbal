 <!-- Begin Page Content -->
 <div class="container-fluid">

<!-- Page Heading -->

    <h1 class="h3 mb-4 text-gray-800"></h1>
            <div class="row">
                <div class="col-lg-6">

                <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newSppModal">Edit Spp</a>

                <?= form_error('spp/read', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                 <?= $this->session->flashdata('message'); ?>

            </div>
         </div>
    </div>

<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- MODAL -->

    <!-- Modal -->
    <div class="modal fade" id="newSppModal" tabindex="-1" aria-labelledby="newSppModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="newSppModalLabel">Edit New Spp</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="<?= base_url('spp/update'); ?>" method="post">
        <div class="modal-body">

        <?php $row = $query->result(); ?>
             
           <div class="form-group">
                  <input type="hidden" class="form-control" id="spp" name="id_spp" placeholder="ID_SPP" value="<?= $row{0}->id_spp ?>">
            </div>

              <div class="form-group">
                  <input type="number" class="form-control" id="spp" name="tahun" placeholder="Spp Tahun" value="<?= $row{0}->tahun ?>">
            </div>

            <div class="form-group">
                  <input type="number" class="form-control" id="spp" name="nominal" placeholder="Spp Nominal" value="<?= $row{0}->nominal ?>">
            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">edit</button>
        </div>
        </form>
        </div>
    </div>
    </div>
