             <!-- Begin Page Content -->
             <div class="container-fluid">

            <!-- Page Heading -->
            <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

            <div class="row">
            <div class="col-lg">

                <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                <?= $this->session->flashdata('message'); ?>

             <a href="" class="btn btn-primary mb-4" data-toggle="modal" data-target="#newKelasModal">Add New Kelas</a>

           
       </div>
     </div>

            </div>

            <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
            
         
        <!-- MODAL -->

    <!-- Modal -->
    <div class="modal fade" id="newKelasModal" tabindex="-1" aria-labelledby="newKelasModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="newKelasModalLabel">Add New Kelas</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="<?= base_url('kelas'); ?>" method="post">
        <div class="modal-body">
             
               <div class="form-group">
                  <input type="number" class="form-control" id="nama_kelas" name="id_kelas" placeholder="ID Kelas">
               </div>

              <div class="form-group">
                  <input type="text" class="form-control" id="nama_kelas" name="nama_kelas" placeholder="Nama Kelas">
              </div>

              <div class="form-group">
                  <input type="text" class="form-control" id="kompetensi_keahlian" name="kompetensi_keahlian" placeholder="Kompetensi Keahlian">
              </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">tambah</button>
        </div>
        </form>
        </div>
    </div>
    </div>



