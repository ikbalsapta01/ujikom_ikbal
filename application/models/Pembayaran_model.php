<?php

class Pembayaran_model extends CI_Model
{
    public function delete($id_pembayaran)
    {
        $this->db->delete('pembayaran', array('id_pembayaran' =>$id_pembayaran)); // Produces: //DELETE FROM mytable // WHERE id = $id
    }

    public function read_by_id($id_pembayaran)
    {
        $query = $this->db->get_where('pembayaran', array('id_pembayaran' => $id_pembayaran));
        return $query;
    }

    public function update()
    {
        $data = array(

            'id_pembayaran' => $this->input->post('id_pembayaran'),
            'id_petugas' => $this->input->post('id_petugas'),
            'nisn' => $this->input->post('nisn'), 
            'tgl_bayar' => $this->input->post('tgl_bayar'),
            'bulan_dibayar' => $this->input->post('bulan_dibayar'),  
            'tahun_dibayar' => $this->input->post('tahun_dibayar'),
            'jumlah_bayar' => $this->input->post('jumlah_bayar'),
            'id_spp' => $this->input->post('id_spp'),        
        );

        $this->db->update('pembayaran', $data, array('id_pembayaran' => $this->input->post('id_pembayaran')));

    }  
}