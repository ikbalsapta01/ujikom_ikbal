<?php

class Kelas_model extends CI_Model
{
    public function delete($id_kelas)
    {
        $this->db->delete('kelas', array('id_kelas' =>$id_kelas)); // Produces: //DELETE FROM mytable // WHERE id = $id
    }

    public function read($id_kelas)
    {
        $query = $this->db->get_where('kelas', array('id_kelas' => $id_kelas));
        return $query;
    }
}