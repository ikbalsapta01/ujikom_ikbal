<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->form_validation->set_rules('nisn', 'nisn', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if($this->form_validation->run() == false) {
            $data['title'] = 'Login page';
        $this->load->view('templates/auth_header');
        $this->load->view('auth/login');
        $this->load->view('templates/auth_footer');
        } else {
            //validasinya success
            $this->_login();
        }
        
       
    }

     private function _login()
     {
        $nisn = $this->input->post('nisn');
        $password = $this->input->post('password');

        $siswa = $this->db->get_where( 'siswa', ['nisn' => $nisn ])->row_array();
        
        
        if($siswa) {

            // jikaa siswanya ada
            //cek password
           if(password_verify($password,$siswa['password'])){
                $data = [
                    'nama' => $siswa['nama'],
                    'role_id' => $siswa['role_id'],
                    'nisn' => $siswa['nisn']
                ];
                $this->session->set_userdata($data);
                if($siswa['role_id'] == 1) {
                    redirect('admin');
                } else {
                    redirect('siswa');
                } 
            
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Password salah, silahkan isi password yang benar!</div>');
                redirect('auth');
            }
          
        } else {
           
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
           Namamu belum terdaftar!</div>');
           redirect('auth');
        }

     } 


    public function registration()
    {

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim|is_unique[siswa.nama]',[
            'is_unique' => 'Nama ini sudah terdaftar!'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password3]', [
            'matches' => 'Password tidak sama!', 
            'min_length' => 'Password terlalu pendek'
        ]);
        
        $this->form_validation->set_rules('password3', 'Repeat Password', 'required|trim|matches[password1]');



        if ($this->form_validation->run() == false) { 
        
        $this->load->view('templates/auth_header');
        $this->load->view('auth/registration');
        $this->load->view('templates/auth_footer');
       } else{
           $data = [
               'nisn' => htmlspecialchars($this->input->post('nisn')),
               'nis' => $this->input->post('nis'),
               'nama' => htmlspecialchars($this->input->post('nama', true)),
               'id_kelas' => $this->input->post('id_kelas'),
               'alamat' => $this->input->post('alamat'),
               'no_telp' => $this->input->post('no_telp'),
               'id_spp' => $this->input->post('id_spp'),
               'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
               'image' => 'default.jpg',
               'role_id' => 2
               
           ];

           $this->db->insert('siswa', $data);
           $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
           Selamat! namamu sudah terdaftar. Silahkan untuk login</div>');
           redirect('auth');
       }

    }

    public function logout()
     {
         $this->session->unset_userdata('nama');
         $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
         Akunmu sudah keluar, terima kasih</div>');
         redirect('auth');
     }


     public function blocked()
     {
         $this->load->view('auth/blocked');
     }

}