<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembayaran_model');
    }

    public function index() {

        $data['title'] = 'Pembayaran';
        $data['siswa'] = $this->db->get_where('siswa', ['nama' => $this->session->userdata('nama')])->row_array();
        

        $data['pembayaran'] = $this->db->get('pembayaran')->result_array();

        $this->form_validation->set_rules('nisn', 'nisn', 'required');
        $this->form_validation->set_rules('tgl_bayar', 'tgl_bayar', 'required');
        $this->form_validation->set_rules('bulan_dibayar', 'bulan_dibayar', 'required');
        $this->form_validation->set_rules('tahun_dibayar', 'tahun_dibayar', 'required');
        $this->form_validation->set_rules('jumlah_bayar', 'jumlah_bayar', 'required');

       if($this->form_validation->run() == false) {

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('pembayaran/index', $data);
            $this->load->view('templates/footer');

        } else {

            // echo "mashook";
            $data = array(

                'id_petugas'      => $this->input->post('id_petugas'),
                'nisn'            => $this->input->post('nisn'),
                'tgl_bayar'       => $this->input->post('tgl_bayar'),
                'bulan_dibayar'   => $this->input->post('bulan_dibayar'),
                'tahun_dibayar'   => $this->input->post('tahun_dibayar'),
                'jumlah_bayar'    => $this->input->post('jumlah_bayar'),
                'id_spp'          => $this->input->post('id_spp'),
            );
            
            $this->db->insert('pembayaran', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Pembayaran spp berhasil!!</div>');
            redirect('pembayaran/index');

        }
       
    }

    public function delete($id_pembayaran)
    {
       $this->pembayaran_model->delete($id_pembayaran);
       $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Data pembayaran telah dihapus!!</div>');
       redirect('pembayaran');
    }

    public function read($id_pembayaran)
    {
        $data['title'] = 'Edit Pembayaran';
        $data['siswa'] = $this->db->get_where('siswa', ['nama' => $this->session->userdata('nama')])->row_array();

        $data['query'] = $this->pembayaran_model->read_by_id($id_pembayaran);
        $data['pembayaran'] = $this->db->get('pembayaran')->result_array();

        $this->form_validation->set_rules('nisn', 'nisn', 'required');
        $this->form_validation->set_rules('tgl_bayar', 'tgl_bayar', 'required');
        $this->form_validation->set_rules('bulan_dibayar', 'bulan_dibayar', 'required');
        $this->form_validation->set_rules('tahun_dibayar', 'tahun_dibayar', 'required');
        $this->form_validation->set_rules('jumlah_bayar', 'jumlah_bayar', 'required');

        if($this->form_validation->run() == false){

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('pembayaran/read', $data);
            $this->load->view('templates/footer');

        } else {

            $data = array(

                'id_petugas'      => $this->input->post('id_petugas'),
                'nisn'            => $this->input->post('nisn'),
                'tgl_bayar'       => $this->input->post('tgl_bayar'),
                'bulan_dibayar'   => $this->input->post('bulan_dibayar'),
                'tahun_dibayar'   => $this->input->post('tahun_dibayar'),
                'jumlah_bayar'    => $this->input->post('jumlah_bayar'),
                'id_spp'          => $this->input->post('id_spp'),
            );

            $this->db->insert('pembayaran', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Edit Pembayaran berhasil!!</div>');
            redirect('pembayaran/index');

        }
        
   }

   public function update()
   {

        $id_petugas = $this->input->post('id_petugas');
        $nisn = $this->input->post('nisn');
        $tgl_bayar = $this->input->post('tgl_bayar');
        $bulan_dibayar = $this->input->post('bulan_dibayar');
        $tahun_dibayar = $this->input->post('tahun_dibayar');
        $jumlah_bayar = $this->input->post('jumlah_bayar');
        $id_spp = $this->input->post('$id_spp');

        $data['query'] = $this->pembayaran_model->update();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Edit Pembayaran berhasil!!</div>');
        redirect('pembayaran/index');
   }

    
    
} 