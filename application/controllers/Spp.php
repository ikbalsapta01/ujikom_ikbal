<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spp extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('spp_model');
    }

    public function index() {
        $data['title'] = 'Spp';
        $data['siswa'] = $this->db->get_where('siswa', ['nama' => $this->session->userdata('nama')])->row_array();

        $data['spp'] = $this->db->get('spp')->result_array();

        $this->form_validation->set_rules('tahun', 'Tahun', 'required');
        $this->form_validation->set_rules('nominal', 'Nominal', 'required');

        if($this->form_validation->run() == false) {

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('spp/index', $data);
            $this->load->view('templates/footer');

        } else {

            $data = array(
                'id_spp'   =>   $this->input->post('id_spp'),
                'tahun'   =>   $this->input->post('tahun'),
                'nominal' => $this->input->post('nominal'),
                
        );
            $this->db->insert('spp', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New Spp Added</div>');
              redirect('spp/index');
        }

   }

   public function delete($id_spp)
   {
       $this->spp_model->delete($id_spp);
       $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Data spp telah dihapus!!</div>');
       redirect('spp');
   }

   public function read($id_spp)
   {
       $data['title'] = 'Edit Spp';
       $data['siswa'] = $this->db->get_where('siswa', ['nama' => $this->session->userdata('nama')])->row_array();

       $data['query'] = $this->spp_model->read_by_id($id_spp);
       $data['spp'] = $this->db->get('spp')->result_array();

        $this->form_validation->set_rules('tahun', 'Tahun', 'required');
        $this->form_validation->set_rules('nominal', 'Nominal', 'required');


       if($this->form_validation->run() == false) {

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('spp/read', $data);
        $this->load->view('templates/footer');

       } else {

            $data = array(
                'id_spp'   =>   $this->input->post('id_spp'),
                'tahun'   =>   $this->input->post('tahun'),
                'nominal' => $this->input->post('nominal'),
            
         );

            $this->db->insert('spp', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Edit spp berhasil!!!</div>');
             redirect('spp/index');
       }
       
   }

   public function update()
   {
       $id_spp = $this->input->post('id_spp');
       $tahun = $this->input->post('tahun');
       $nominal = $this->input->post('nominal');

       $data['query'] = $this->spp_model->update();
       redirect('spp/index');
   }

}