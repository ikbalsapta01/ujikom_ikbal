<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('role_model');
    }
    
    public function index()
      {
          $data['title'] = 'Dashboard';
          $data['siswa'] = $this->db->get_where('siswa', ['nama' => $this->session->userdata('nama')])->row_array();

          $this->load->view('templates/header', $data);
          $this->load->view('templates/sidebar', $data);
          $this->load->view('templates/topbar', $data);
          $this->load->view('admin/index', $data);
          $this->load->view('templates/footer');
      }

      public function role()
      {
          $data['title'] = 'Role';
          $data['siswa'] = $this->db->get_where('siswa', ['nama' => $this->session->userdata('nama')])->row_array();
           
          $data['role'] = $this->db->get('user_role')->result_array();

          $this->form_validation->set_rules('role', 'role', 'required');

         if($this->form_validation->run() == false) {
        
                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('admin/role', $data);
                $this->load->view('templates/footer');

            } else {

                $data = array(

                    'role'   =>   $this->input->post('role'),
                    
           );
                $this->db->insert('user_role', $data);
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New Role Added</div>');
                  redirect('admin/role');
            }
          
        }

      public function roleAccess($role_id)
      {
          $data['title'] = 'Role Access';
          $data['siswa'] = $this->db->get_where('siswa', ['nama' => $this->session->userdata('nama')])->row_array();
          

          $data['role'] = $this->db->get_where('user_role', ['id' => $role_id])->row_array();

          $this->db->where('id !=', 1);
          $data['menu'] = $this->db->get('user_menu')->result_array();

          $this->load->view('templates/header', $data);
          $this->load->view('templates/sidebar', $data);
          $this->load->view('templates/topbar', $data);
          $this->load->view('admin/role-access', $data);
          $this->load->view('templates/footer');
      }

      public function delete($id)
    {
       $this->role_model->delete($id);
       $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Data role telah dihapus!!</div>');
       redirect('admin/role');
    }
    
}