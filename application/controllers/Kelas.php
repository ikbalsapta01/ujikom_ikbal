<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('kelas_model');
        
    }

    public function index() {

        $data['title'] = 'Kelas';
        $data['siswa'] = $this->db->get_where('siswa', ['nama' => $this->session->userdata('nama')])->row_array();

        $data['kelas'] = $this->db->get('kelas');

        $this->form_validation->set_rules('nama_kelas', 'nama_kelas', 'required');
        $this->form_validation->set_rules('kompetensi_keahlian', 'kompetensi_keahlian', 'required');

        if($this->form_validation->run() == false) {

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('kelas/index', $data);
            $this->load->view('templates/footer');

        } else {
                $this->db->insert('kelas', ['nama_kelas' => $this->input->post('nama_kelas'),'kompetensi_keahlian' => $this->input->post('kompetensi_keahlian')]);
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New Kelas Added</div>');
                redirect('kelas');
        } 
          
    } 

    public function delete($id_kelas)
    {
       $this->kelas_model->delete($id_kelas);
       $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Data kelas telah dihapus!!</div>');
       redirect('kelas');
    }

    public function read($id_kelas)
    {
        $data['title'] = 'Edit Kelas';
        $data['siswa'] = $this->db->get_where('siswa', ['nama' => $this->session->userdata('nama')])->row_array();
        echo 'Selamat datang ' . $data['siswa']['nama'];

        $data['kelas'] = $this->db->get('kelas')->result_array();
        $data['query'] = $this->spp_model->read_by_id($id_kelas);

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('kelas/read', $data);
            $this->load->view('templates/footer');
    }
           
}