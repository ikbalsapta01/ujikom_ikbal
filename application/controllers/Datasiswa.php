<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datasiswa extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        
    }

    public function index() 
    {
        $data['title'] = 'Data Siswa';
        $data['siswa'] = $this->db->get_where('siswa', ['nama' => $this->session->userdata('nama')])->row_array();
        
        
        $data['datasiswa'] = $this->db->get('siswa')->result_array();

        $this->form_validation->set_rules('nisn', 'nisn', 'required');
        $this->form_validation->set_rules('nis', 'nis', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('alamat', 'alamat', 'required');
        $this->form_validation->set_rules('no_telp', 'no_telp', 'required');

        if($this->form_validation->run() == false) {
            
          $this->load->view('templates/header', $data);
          $this->load->view('templates/sidebar', $data);
          $this->load->view('templates/topbar', $data);
          $this->load->view('datasiswa/index', $data);
          $this->load->view('templates/footer');
          
        } else {

            $data = array(
                
                'nisn'   =>   $this->input->post('nisn'),
                'nis'   =>   $this->input->post('nis'),
                'nama'   =>   $this->input->post('nama'),
                'alamat'   =>   $this->input->post('alamat'),
                'no_telp'   =>   $this->input->post('no_telp'),
                              
             );

             $this->db->insert('siswa', $data);
             $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New data siswa added</div>');
               redirect('datasiswa');

        }
          
    }
}
