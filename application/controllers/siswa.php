<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        
    }

    public function index()
      {
          
          $data['title'] = 'My profile';
          $data['siswa'] = $this->db->get_where('siswa', ['nisn' => $this->session->userdata('nisn')])->row_array();

          $this->load->view('templates/header', $data);
          $this->load->view('templates/sidebar', $data);
          $this->load->view('templates/topbar', $data);
          $this->load->view('siswa/index', $data);
          $this->load->view('templates/footer');
      }

      public function edit()
       {
         $data['title'] = 'Edit Profile';
         $data['siswa'] = $this->db->get_where('siswa', ['nisn' => $this->session->userdata('nisn')])->row_array();

         $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
         $this->form_validation->set_rules('alamat', 'alamat', 'required|trim');

        if($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('siswa/edit', $data);
            $this->load->view('templates/footer');

        } else {

            $nama = $this->input->post('nama');
            $nisn = $this->input->post('nisn');
            $alamat = $this->input->post('alamat');

            //cek jika ada gambar yang akan diupload
            $upload_image = $_FILES['image']['name'];

            if($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                // $config['max_size']      = '2048';
                $config['upload_path']   = './assets/img/profile/';

                $this->load->library('upload', $config);

                if($this->upload->do_upload('image')) {
                    $new_image = $this->upload->data('file_name');
                    // var_dump($new_image);
                    $this->db->set('image', $new_image);
                    $this->db->set('nama', $nama); 
                    $this->db->set('alamat', $alamat);
                    $this->db->where('nisn',$this->session->userdata('nisn'));
                    $this->db->update('siswa');

                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Profile sudah terupdate!</div>');
                    redirect('siswa/edit');
                } else {
                    echo $this->upload->dispay_errors();
                }
            } else {

                    $this->db->set('alamat', $alamat);
                    $this->db->set('nama', $nama); 
                    $this->db->where('nisn',$this->session->userdata('nisn'));
                    $this->db->update('siswa');
                    
                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Profile sudah terupdate!</div>');
                    redirect('siswa/edit');
            }

     
        }
    }

}