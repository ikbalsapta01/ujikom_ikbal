<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaransaya extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        
    }

    public function index() {

        $data['title'] = 'Pembayaran Saya';
        $data['siswa'] = $this->db->get_where('siswa', ['nama' => $this->session->userdata('nama')])->row_array();
        
        $data['pembayaran'] = $this->db->get('pembayaran')->result_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('pembayaransaya/index', $data);
        $this->load->view('templates/footer');

    }
}